#CA3 Part 1 - Virtualization with Vagrant
--------------

## Goals
Practice with VirtualBox using the same projects from the previous assignments.
--------------

##A - Analysis, Design and Implementation

###A1 - Create Virtual Machine (VM)

First we need to create a new virtual machine, following the instructions from the previous lecture, with the following specifications:

* Minimal installation media of ***Ubuntu 18.04***. 
* **_2048 MB_** RAM
* **_10 GB_** storage
* Network Adapter 1 as ***NAT***
* Network Adapter 2 as ***Host-only Adapter (vboxnet0)***, with IPv4 Address **_192.168.56.1_**
    
--------------
###A2 - Setup VM

After creating the VM, wee need to start it and install Linux (Ubuntu) on it, still following the lecture slides.
When the installation is completed we need to setup our VM which is running now Linux (Ubuntu):

* Install the network tools with:

        sudo apt install net-tools
    
* Launch network configuration ﬁle with:

        sudo nano /etc/netplan/01-netcfg.yaml

* Edit the file to setup the IP as:
    
        network:
           version: 2
           renderer: networkd
           ethernets:
               enp0s3:
                dhcp4: yes
               enp0s8:
                addresses:
                    - 192.168.56.5/24

* Apply the new changes with:

        sudo netplan apply
        
* Install openssh-server with:

        sudo apt install openssh-server
 
* Launch ssh configuration file with:

        sudo nano /etc/ssh/sshd_conﬁg
        
* Enable password authentication for ssh with by uncomment the line:

        PasswordAuthentication yes

* Restart ssh service with:

        sudo service ssh restart

--------------
###A3 - Try to access VM with ssh

Now that we configured the VM network, it was now possible to remotely access our VM using ssh from comandline:

    ssh frederico@192.168.56.5
    
--------------
###A4 - Install necessary packages in the VM
To be able to run the previous assignments in the VM, we need to install some packages:

    sudo apt install git
    sudo apt install openjdk-8-jdk-headless
    sudo apt install maven
    sudo apt install gradle
    
--------------
###A5 - Clone testing repository into the VM

Now that we have git installed we need to clone our repository into our VM:

    git clone https://FredericoVasconcelos@bitbucket.org/FredericoVasconcelos/devops-19-20-b-1191764.git
 
 Now we can run the applications developed for the the previous assignments.    
 
--------------
###A6 - Run ca1

First we need to change into the correct directory:

    cd devops-19-20-b-1191764/ca1/basic
    
Before we run the application we need to give execution permission to the maven executable:

    chmod +x mvnw

Now we can launch the application:

    ./mvnw spring-boot:run
    
Since no errors were found, we need to debug the client part of the application:

    http://192.168.56.5:8080

We can verify that the table developed for ca1 is displayed on the screen and conclude this part of the exercise.

--------------
###A7 - Run ca2-part1

Again, we need to change into the correct directory:
  
      cd devops-19-20-b-1191764/ca2_Part1
      
Next, we need to give execution permission to the gradle executable:
   
       chmod +x gradlew
   
Now, we create a build with:
   
       ./gradlew build
       
After the build is created we need to run the server:
   
       java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
   
Now we must run the client on the host machine but before that, we need to edit the `build.gradle` file of ca2-part1. 
The task `runClient` is set to the wrong address. Instead of localhost we need to give it the IP of our VM:
  
  	task runClient(...){
  		(...)
  		args '192.168.56.5', '59001'
  	}
  
Now, we can run the client in the host machine:
  
      gradlew runClient
  
The chat app is launched in the host machine. This part of the exercise is now concluded.

--------------
###A8 - Run ca2-part2

Once again, we need to change into the correct directory:

    cd devops-19-20-b-1191764/ca2_Part2/demo

Next, we need to give execution permission to the gradle executable:
                                   
    chmod +x gradlew

Now, we create a build with:

    ./gradlew clean build

After the build is created we can run the application:

    ./gradlew bootRun

We can now debug the client part of the application:

    http://192.168.56.5:8080

We can verify that the table developed for ca2 is displayed on the screen and conclude this part of the exercise.
       
--------------
### Conclusion
After the completion of this report, a new tag was created to mark this assignment, and the report and the tag were uploaded to the repository: 

    git tag ca3-part1
    git push origin ca3-part1
    
    git add README.md
    git commit -a -m "README.md for ca3_Part1"
    git push