#CA4 - Containers with Docker
--------------

## Goals
Use Docker to setup a containerized environment to
execute one's version of the gradle version of spring basic tutorial application
(developed in ca2_part2)

--------------
##A - Analysis, Design and Implementation

###A1 - Download initial solution

First, we need to download the example provided from https://bitbucket.org/atb/docker-compose-spring-tut-demo/ and 
extract it into the local ca4 folder and then upload it to the repository.
    
--------------
###A2 - Edit the web Dockerfile

We need to modify the *Dockerfile* located inside the web folder, in order to link it to our own repository, 
to make use of the *ca2_Part2_copy*, give execution permissions to the gradlew file, change the *.war* filename 
to match the that is going to be generated after build:

    FROM tomcat
    
    RUN apt-get update -y
    
    RUN apt-get install -f
    
    RUN apt-get install git -y
    
    RUN apt-get install nodejs -y
    
    RUN apt-get install npm -y
    
    RUN mkdir -p /tmp/build
    
    WORKDIR /tmp/build/
    
    RUN git clone https://FredericoVasconcelos@bitbucket.org/FredericoVasconcelos/devops-19-20-b-1191764.git
    
    WORKDIR /tmp/build/devops-19-20-b-1191764/ca2_Part2_copy/demo
    
    RUN chmod u+x gradlew
    
    RUN ./gradlew clean build
    
    RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
    
    EXPOSE 8080

--------------
###A3 - Build and run
        
After step A2, and having previously installed Docker on the PC, in the Docker Quickstart Terminal we must navigate to 
the folder where *docker-compose.yml* is located. In our case it is our local ca4 folder, and then we need to run 
the following command to perform the build:

    `docker-compose build`

Afterwards, we need to start the containers with:

    `docker-compose up`

Now we can access the localhost to check if our spring web application is working using the following URL:

    http://localhost:8080/demo-0.0.1-SNAPSHOT/
    
Note that because docker toolbox is being used for this assignment, the URL had to be changed to substitute 'localhost'
 with the ip address where Virtual Machine executes Docker. 
In order to check this ip address, the following command was executed:

`docker-machine ip default`

So the URL was changed to:

    http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT/

And we can also open the H2 console using the following URL:

    http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT/h2-console

In the field *JDBC URL* needs to be changed to:

    jdbc:h2:tcp://192.168.33.11:9092/./jpadb

--------------
###A4 - Publish the db and web images to Docker Hub
               
To publish the images to Docker Hub, we need to:
                 
1. Create an account at https://hub.docker.com/ and log in;
2. Still at https://hub.docker.com/, create a Repository.
4. Log in to the DockerHub from the command line with: `docker login` 
and enter our credentials when asked
5. We can check the image ID with the command: 
    `docker images`
6. Then we need to tag the image we want to upload, with:
    `docker tag <IMAGEID> 1191764/devops-19-20-b-1191764:<TAG>`
7. Finally, to push the image we just tagged to the Docker Hub repository:
    `docker push 1191764/devops-19-20-b-1191764:<TAG>`
                 
Now we have our images accessible in our Docker Hub repository (`1191764/devops-19-20-b-1191764`). For this project 
the `db` image was tagged `db1.0` and the `web` image was tagged `1.0`.

--------------
###A5 - Backup the database file using a volume with the db container
        
First we need to "enter" our db container:
        
    `docker-compose exec db bash`
            
Inside a container you can run any linux command (if available in the docker image). 
So now we can copy the database file to the *data* folder:
        
    `cp /usr/src/app/jpadb.mv.db /usr/src/data`
            
Note: these instructions were followed and in the terminal after the second command to copy the file, I navigated to the 
*data* folder and confirmed that the file had in fact been copied using the *ls* command, but in the windows explorer the
file did not show.

Update: With the help of the professor it we discovered that the problem resided in the path of the project, which was 
in `/c` and the path of the *VM* shared folder which was set to `/users`. We tried adding a new shared folder to the *VM*
 with the correct path, but it still wouldn't work. Maybe the path was incomplete or the original shared folder was 
 superimposing itself to the newly created one. 
 Ultimately, to avoid more trial and error with the paths and shared folders, the project was changed to the `/Documents`
 folder and the A5 steps repeated this time successfully. The database file was copied to the `/data` folder and to 
 upload it to the repository, the `.gitignore` file had to be altered, commenting the line `/data/*.db`.

--------------
### Conclusion
After the completion of this report, a new tag was created to mark this assignment, and the report and the tag were uploaded to the repository: 

    git add README.md
    git commit -m "README.md for ca4"
    git push
    
    git tag ca4
    git push origin ca4
