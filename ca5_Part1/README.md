#CA5 Part 1 - Jenkins
--------------

## Goals

Practice with Jenkins using the "gradle basic demo" project that is already be present in the individual repository.

--------------
##A - Analysis, Design and Implementation

###A1 - Download and install Jenkins war package

Download the war package from https://www.jenkins.io/doc/book/installing/#war-file . Open a new terminal/console, 
navigate to the folder where the war file is and run the command:

    java -jar jenkins.war
    
After the installation a password is displays on the terminal. It needs to be used to setup the Jenkins account.

During configuration, accept all suggested plugins. The administrator account was created with the user 
*1191764@isep.ipp.pt* and User ID *fnpcv*.

    
--------------
###A2 - Create repository credentials

We need to tell Jenkins what credentials to use when accessing our repository. 

In Jenkins, wee need to go to "Credentials" and create new credentials with the username and password of our Bitbucket 
repository.

The field ID is used in scripts to refer to these credentials. The ID used was *1191764-bitbucket*. The Jenkinsfile 
created refers to this ID.

--------------
###A3 - Create a new Job of type Pipeline
        
Go to *New Item* and chose *Pipeline*.

As a first experiment a new pipeline named *devops-ca5_Part1-local* was created. In the configuration page of the Job,
 in the *Pipeline* section, the *Definition* parameter need to be set to *Pipeline script*. The script code used was 
 from the class pdf. 
 
Some changes are necessary to connect it to the repository:

- In stage *Checkout* the credentials ID was changed to match the one that refers to my repository;
- Still in stage *Checkout* the url was changed to match my repository as well;
- The *Build* stage was changed to *Assemble* stage, because the build task of gradle would also execute the tests. 
Instead the assemble task was used.
- A *Test* stage was added in order to run the tests separately;
- Click the *Save/Apply* button.
 
In the *devops-ca5_Part1-local* pipeline page, click the *Build Now* button.
After having a functional script, I copied the code into a new Jenkinsfile and uploaded it to my repository.

--------------
###A4 - Running a Jenkinsfile from the repository

To run this Jenkinsfile from the repository a new Job was created. This time:
- The job was named *devops-ca5_Part1-remote* ;
- In the *Pipeline* section, the *Definition* parameter was set to *Pipeline script from SCM*;
- The *SCM* parameter was set to *git*;
- The repository url was set to my repository;
- The credentials were set to the ID of the credentials that refer to my repository;
- The *Script Path* was set to the Jenkinsfile path in my repository: *ca5_Part1/Jenkinsfile*;

After the configuration is complete, the pipeline was saved and a build was made clicking the *Build Now* button. 

--------------
### Conclusion
After the completion of this report, a new tag was created to mark this assignment, and the report and the tag were 
uploaded to the repository: 

    git add README.md
    git commit -m "README.md for ca5_Part1"
    git push
    
    git tag ca5-part1
    git push origin ca5-part1
