#CA3 Part 2 - Virtualization with Vagrant
--------------

## Goals
Use Vagrant to setup a virtual environment to execute the tutorial spring boot application, gradle "basic" version
(developed in ca2_part2)

--------------
##A - Analysis, Design and Implementation

###A1 - Download initial solution

First, we need to download the initial solution from https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/.
    
--------------
###A2 - Make a Copy ca2_part2

As a personal choice, a copy of *ca2_Part2* was created called *ca2_Part2_copy* in order to be able to work without the 
risk of altering the original. 

    git add ca2_Part2_copy
    git commit -a -m "copia de ca2_Part2 para ser usado pelo ca3_part2"
    git push 

--------------
###A3 -  Copy Vagrantfile to repository
        
Now we need copy the Vagrantfile from the initial solution into a *ca3-part2* folder in the local copy of our repository and then upload it:
        
    git add Vagrantfile
    git commit -m "changed Vagrantfile to comply with spring application"
    git push 
    
Note: by distraction, this commit was only made almost at the end of the assignment. 

--------------
###A4 - Update Vagrantfile configuration

We need to substitute the link of the repository for our own. We aso need to change the application path, add a line to 
give execution permitions to the gradle executable as well as change the name of the war file. 
        
    # Change the following command to clone your own repository!
    git clone https://FredericoVasconcelos@bitbucket.org/FredericoVasconcelos/devops-19-20-b-1191764.git
    cd devops-19-20-b-1191764/ca2_Part2_copy/demo
    chmod +x gradlew
    ./gradlew clean build
    # To deploy the war file to tomcat8 do the following command:
    sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
    
Note that the repository access level was changed from private to public to avoid credential issues.
    
--------------
###A5 - Update Spring Application to handle Vagrant

To update ca2_Part2_copy application to handle Vagrant, and following the instructions provided, several files were 
changed to be in accordance to the project from https://bitbucket.org/atb/tut-basic-gradle.

--------------
###A6 - Launch application

To launch the application we need to, in comandline, navigate to the ca3_Part2 folder. Once there we use the following command:

    vagrant up
            
We can now debug the client part of the application:

        http://localhost:8080/demo-0.0.1-SNAPSHOT/
        http://192.168.56.5:8080/demo-0.0.1-SNAPSHOT/

We can also open the H2 console using one of the following urls:

    http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console
    http://192.168.56.5:8080/demo-0.0.1-SNAPSHOT/h2-console

--------------
### Conclusion
After the completion of this report, a new tag was created to mark this assignment, and the report and the tag were uploaded to the repository: 

    git tag ca3-part2
    git push origin ca3-part2
    
    git add README.md
    git commit -m "README.md for ca3_Part2"
    git push
