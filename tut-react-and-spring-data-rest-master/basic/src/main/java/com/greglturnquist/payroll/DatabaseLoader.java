/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Component // <1>
public class DatabaseLoader implements CommandLineRunner { // <2>

	private final EmployeeRepository repository;

	@Autowired // <3>
	public DatabaseLoader(EmployeeRepository repository) {
		this.repository = repository;
	}

	@Override
	public void run(String... strings) throws Exception { // <4>
		this.repository.save(new Employee("Frodo", "Baggins", "Brave hobbit", "Ring bearer"));
		this.repository.save(new Employee("Samwise", "Gamgee", "Brave hobbit", "Sidekick"));
		this.repository.save(new Employee("Gandalf", "The Grey", "A wizard is never late. He arrives precisely at the time he intended to.", "Wizard"));
		this.repository.save(new Employee("Aragorn II", "Elessar Telcontar", "Aragorn son of Arathorn, called Elessar, the Elfstone, Dúnadan, the heir of Isildur Elendil's son of Gondor.","The true King"));
		this.repository.save(new Employee("Boromir", "Son of Denethor", "Captain of the White Tower", "The heir of Denethor II"));

	}
}
// end::code[]
