#CA5 Part 2 - Jenkins
--------------

## Goals

Create a pipeline in Jenkins to build the tutorial spring boot application, gradle "basic" version 
(developed in CA2, Part2).

--------------
##A - Analysis, Design and Implementation

###A1 - Add unit tests to ca2_Part2

ca2_Part2 did not have any unit tests, so it was necessary to create some and upload them to the repository. Two simple 
tests were added for *Employee.java*.
    
--------------
###A2 - Create a simple pipeline

First Jenkins needs to be executed. In a terminal console used the following command;

    java -jar jenkins.war

Since most stages on this new pipeline were going to be similar to the ones in the pipeline created for ca5_Part1, I
used the previous one and made the necessary adjustments to each stage:

- `Checkout` - no changes necessary;
- `Assemble` - the path to the application had to be changed to match ca2_part2_copy, since this was the one with the 
necessary modifications to use Tomcat and generate a war file;
- `Test` - here the path to the application as well as the *junit* step output path had to be changed to match ca2_part2_copy;
- `Archive` - the *archiveArtifacts* step had to be updated to match ca2_part2_copy as well;

--------------
###A3 - Add the *Javadoc* stage to the pipeline
        
This was a new part, relatively to the previous assignment and so, to achieve the desired result, the following code 
was added to the pipeline:

    stage ('Javadocs'){
        steps {
            echo 'Creating Javadocs'
            bat 'cd ca2_Part2_copy/demo/ & gradlew javadoc'

            publishHTML([
                        reportName: 'Javadoc',
                        reportDir: 'ca2_Part2_copy/demo/build/docs/javadoc/',
                        reportFiles: 'index.html',
                        keepAll: false,
                        alwaysLinkToLastBuild: false,
                        allowMissing: false
                        ])
        }
    }
        
It is important to note that for this stage to pass, it was necessary to install *HTML Publisher* plugin in Jenkins.

After successfully passing this stage, it is possible to access the documents in the *Workspaces* tab in Jenkins.

--------------
###A4 - Add the *Publish image* stage

Since this stage is going to create a docker image and upload it to my Docker-Hub repository, a new set of credentials 
for my Docker-Hub repository ware created in Jenkins and referred in the stage code. This stage was created with the 
following code:

    stage ('Publish Image') {
        steps {
            echo 'Publishing Docker image'
            script {
        docker.withRegistry('https://index.docker.io/v1/', 'docker-hub_ca5_part2') {
                               def customImage =
                               docker.build("1191764/devops-19-20-b-1191764:ca5_Part2_${env.BUILD_ID}", "ca5_Part2")
                               customImage.push()
                    }
            }
        }
    }
    
- `docker.withRegistry` - In order to use a custom Docker Registry. Two arguments ar passed: the custom Registry URL 
and the Credentials ID as a second argument;
- `docker.build` - composed of two string arguments. The first argument is composed of the name of the repository followed by a colon sign and the respective 
tag to be created. The second argument is the path to where the Dockerfile is located in the repository;
- `customImage.push()` - sends the image to the repository.

But before attempting to build, it was necessary to create a Dockerfile that ran Tomcat and the spring application and 
generated th war file. Since we already had one that did exactly that, the Dockerfile from the web container from ca4 
was copied into ca5_Part2 folder and the uploaded to the repository so that our pipeline can make use of it.

Also important to note, Docker must be running for this stage to run successfully.

After successfully running the pipeline, the new docker image was created with the tag identical to the build ID 
`${env.BUILD_ID}`.
  
It can be accessed through the Docker-Hub repository link:

    https://hub.docker.com/repository/docker/1191764/devops-19-20-b-1191764

upload do jenkinsfile e mudar a pipeline pa correr remotamente

--------------
###A5 - Create a Jenkinsfile and a new pipeline

After successfully completing the previous steps, the pipeline code was copied into a *Jenkinsfile* that was created 
in the same folder as the previously mentioned Dockerfile and then uploaded to the Bitbucket repository.

Then a new pipeline named *devops-ca5_part2-remote* was created:

- In the *Pipeline* section, the *Definition* parameter was set to *Pipeline script from SCM*;
- The *SCM* parameter was set to *git*;
- The repository url was set to my repository;
- The credentials were set to the ID of the credentials that refer to my Bitbucket repository;
- The *Script Path* was set to the Jenkinsfile path in my repository: *ca5_Part2/Jenkinsfile*;

A new build was attempted with success but this time, the name of the docker image was changed to include a prefix 
before the build ID:

    ca5_Part2_${env.BUILD_ID}
    
The final name of this new docker image is *ca5_Part2_25*, and can be consulted through the Docker-Hub repository link:
                                                                                
    https://hub.docker.com/repository/docker/1191764/devops-19-20-b-1191764

--------------
#B. Analysis and implementation of an alternative to Jenkins

--------------
###B1 - Jenkins-Buddy analysis

As a web based service Buddy might offer some advantages such as no maintenance needed by the user, no installation 
required or much more stable integrations since each one is tested in-house.

On the other hand, the plethora of open source plugins that the Jenkins community offers, means that if an integration 
doesn't exist, someone in the community might have already created one. Ultimately if nonexistent at all, you create the
 plugin yourself. Even so, the community plugins may become unstable due to lack of maintenance and might interfere with 
 each other in unexpected ways.
 
Source: [here](https://hackernoon.com/buddy-vs-jenkins-mt4v32u2)

--------------
###B2 - Implementation

This implementation is based on the approach my colleagues Rui Rosendo and António Silva took, and whose contribution is 
greatly appreciated.

The first step was to create a Buddy account and link it with my Bitbucket repository.

--------------
####B2.1 - Create a new pipeline

After the account was created and linked to my Bitbucket repository, I needed to create a new pipeline. To do that, I:

- Clicked the `Add a new pipeline` button;
- Named it *ca5-part2_buddy*;
- Set `Trigger Mode` to *Manual* because I only want it to be run when I manually tell Buddy to run it;
- Set the branch to *master*;
- When all previous settings were configured, clicked the `Add a new pipeline` button.

--------------
####B2.1 - Replicate the *Assemble stage*

The equivalent to Jenkins stages in Buddy are Actions. To add an action that replicated the *Assemble stage* from 
Jenkins, I completed the following steps:

- Add new action to *ca5-part2_buddy* pipeline;
- Add a `Gradle` action, that can be located under the *BUILD TOOLS & TASK RUNNERS* section;
- In the script section, clear the default code that is present and add the following:


    cd ca2_Part2_copy/demo
    gradle clean assemble
    
- Click the `Add this action` button.

--------------
####B2.2 - Replicate the *Test stage*

Now the screen is a little different. To add a new action, there is no `Add new action` button. Instead there are two 
`+` buttons, one above and one below the icon of the *Gradle* action. These are the buttons that add a new action before 
and after the *Gradle* action respectively. 
To replicate the *Test stage* I added a new *Gradle* action after the first one and added the following code:

    cd ca2_Part2_copy/demo
    gradle test

Now the files generated by the build can be accessed through the specific pipeline page, in the `Filesystem` tab. We can
 access the tests reports here.

--------------
####B2.3 - Replicate the *Archive stage*

This stage is automatically replicated since we can access the filesystem of the pipeline and find the *.war* file there.

--------------
####B2.4 - Replicate the *Publish stage*

In order to fully replicate *Publish stage*, two actions are needed: one to build the docker image and a second to push 
it to my Dockerhub repository.

For the first one:

- Add a `Build image` action located under the *Docker* section;
- Select the Dockerfile to be used, present in the Bitbucket repository;
- Click the `Add this action` button.

For the second one:

- Add a `Push image` action located under the *Docker* section;
- Select `Image built in the precious action`;
- In the `Docker registry` select `Docker Hub`;
- Input the credentials to the Dockerhub repository (account name and password);
- Input the repository name. In my case `1191764/devops-19-20-b-1191764`;
- Define a tag for the image to be pushed to the repository. In my case `ca5_Part2_buddy`.


--------------
####B2.5 - Run pipeline

After all the previous configurations were completed I ran the pipeline and all the stages, or better said, all the 
actions, executed successfully.

--------------
### Conclusion
After the completion of this report, a new tag was created to mark this assignment, and the report and the tag were 
uploaded to the repository: 

    git add README.md
    git commit -m "README.md for ca5_Part2"
    git push
    
    git tag ca5-part2
    git push origin ca5-part2

