package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    /**
     * Test for Employee Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for the constructor - happy case")
    void EmployeeConstructorHappyCaseTest() {
        Employee testEmployee = new Employee("test", "employee", "just a test", "tester", "test1@employeetest.com");
        assertTrue(testEmployee instanceof Employee);
    }

    /**
     * Test for Employee Constructor
     * Exception with invalid first name - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with null first name")
    void AddressConstructorEnsureExceptionWithNullFirstNameTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee testEmployee = new Employee(null, "employee", "just a test", "tester", "test1");
        });
    }

    /**
     * Test for Employee Constructor
     * Exception with invalid first name - empty string
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with empty first name")
    void AddressConstructorEnsureExceptionWithEmptyFirstNameTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee testEmployee = new Employee("    ", "employee", "just a test", "tester", "test1");
        });
    }

    /**
     * Test for Employee Constructor
     * Exception with invalid last name - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with null last name")
    void AddressConstructorEnsureExceptionWithNullLastNameTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee testEmployee = new Employee("test", null, "just a test", "tester", "test1");
        });
    }

    /**
     * Test for Employee Constructor
     * Exception with invalid last name - empty string
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with empty last name")
    void AddressConstructorEnsureExceptionWithEmptyLastNameTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee testEmployee = new Employee("test", "    ", "just a test", "tester", "test1");
        });
    }

    /**
     * Test for Employee Constructor
     * Exception with invalid description - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with null description")
    void AddressConstructorEnsureExceptionWithNullDescriptionTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee testEmployee = new Employee("test", "employee", null, "tester", "test1");
        });
    }

    /**
     * Test for Employee Constructor
     * Exception with invalid description - empty string
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with empty description")
    void AddressConstructorEnsureExceptionWithEmptyDescriptionTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee testEmployee = new Employee("test", "employee", "    ", "tester", "test1");
        });
    }

    /**
     * Test for Employee Constructor
     * Exception with invalid job title - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with null job title")
    void AddressConstructorEnsureExceptionWithNullJobTitleTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee testEmployee = new Employee("test", "employee", "just a test", null, "test1");
        });
    }

    /**
     * Test for Employee Constructor
     * Exception with invalid job title - empty string
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with empty job title")
    void AddressConstructorEnsureExceptionWithEmptyJobTitleTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee testEmployee = new Employee("test", "employee", "just a test", "    ", "test1");
        });
    }

    /**
     * Test for Employee Constructor
     * Exception with invalid email - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with null email")
    void AddressConstructorEnsureExceptionWithNullEmailTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee testEmployee = new Employee("test", "employee", "just a test", "tester", null);
        });
    }

    /**
     * Test for Employee Constructor
     * Exception with invalid email - empty string
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with empty email")
    void AddressConstructorEnsureExceptionWithEmptyEmailTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee testEmployee = new Employee("test", "employee", "just a test", "tester", " ");
        });
    }

    /**
     * Test for Employee Constructor
     * Exception with invalid email - missing @ sign in email field
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with missing @ sign in email field")
    void AddressConstructorEnsureExceptionWhenMissingAtSignTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee testEmployee = new Employee("test", "employee", "just a test", "tester", "test1.employeetest.com");
        });
    }

    /**
     * Test for Employee Constructor
     * Exception with invalid email - missing "." sign in email field
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with missing '.' sign in email field")
    void AddressConstructorEnsureExceptionWhenMissingDotSignTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee testEmployee = new Employee("test", "employee", "just a test", "tester", "test1@employeetestcom");
        });
    }
}