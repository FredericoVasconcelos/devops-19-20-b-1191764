#A. Analysis, Design and Implementation

    Nota: para esta implementação, foi usado o terminal do IntelliJ, 

###A.1 - Tag the initial version as v1.2.0:

To create a tag, the following commands should be used in the respective order:

    git tag v1.2.0           - to create the actual tag
    git push origin v1.2.0   - to push the tag to the repository
   
###A.2 - You should create a branch called email-field:

To create a new branch the following commands

    git branch email-field     - to create the a new branch called "email-field"
    git checkout email-field   - to change from the current working branch to "email-field" branch

Alternatively, a new branch can be created and automatically changed into to immediately start working in it:

    git checkout -b email-field

###A.3 - You should add support for email field:

- First a new attribute String email was added to the class *Employee.java*, and a new parameter *"String email"* was added to the class constructor; 
- Then a new table header "Email" and the respective cell or table data were added to *app.js*;
- Finally email parameters were added to all the entries in the *DatabaseLoader.java* file to match the missing parameter.

###A.4 - You should also add unit tests for testing the creation of Employees and the validation of its attributes (for instance, no null/empty values):

- A new *EmployeeTest.java* class was created and several tests were added to validate the creation of Employee objects (null values and empty strings or strings comprised of just spaces);
- *Employee.java* class constructor attribute instantiation was changed to set method calls of respective attributes;
- A condition to verify invalid parameters was added to each set method along with an exception to be thrown in case null or empty parameters are received.

###A.5 - You should debug the server and client parts of the solution:
In order to verify if the application was working correctly, the command *mvnw spring-boot_run* was run in the IDE terminal. *localhost:8080* was then accessed through an internet browser to verify that the table Employees was loaded without problems.                 
    
###A.6 - When the new feature is completed (and tested) the code should be merged with the master and a new tag should be created (e.g, v1.3.0).

To upload the support for email field the following commands were run: 

    git commit -a -m "fix #issueNumber message"
    git push origin email-field 
    git checkout -b master;
    git merge email-field;
    git tag v1.3.0
    git push origin v1.3.0

Additionally, the recently merged branch could be deleted if it was not anymore. For that the following command is used:

    git branch -d email-field
    
###A.7 - Create a branch called fix-invalid-email. (e.g., an email must have the "@" sign):

First to create the new branch and change into it, the following command aws used:

    git checkout -b fix-invalid-email
    
After that, a condition to verify if the email received has an "@" sign was added to the *setEmail* method which already verified for a null and empty/just spaces string cases. Additionally, another condition to verify if a "." sign was present was added, since an email always contains at least one "." sign.

Once again, to verify and debug the application the command *"mvnw spring-boot_run"* was run in the IDE and then *localhost:8080* was accessed through an internet browser to verify that the table Employees was loaded without problems.

Once the fix was working correctly, the next steps are similar to the ones in section 6:

    git commit -a -m “fix #issueNumber” 
    git push origin fix-invalid-email
    git checkout -b master
    git merge fix-invalid-email
    git tag v1.3.1
    git push origin v1.3.1

Again, if not needed anymore, the merged branch could be deleted: 

    git branch -d fix-invalid-email

###A.8 - At the end of the assignment mark your repository with the tag ca1.
After the conclusion of this report, the following commands were used:

    git commit -a -m “fix #issueNumber” 
    git push origin master
    git tag ca1
    git push origin ca1
    
#B. Analysis of an Alternative

There are several alternatives to git. The one considered for this assignment was *Subversion*.

*Subversion*, or SVN for short, is a *Centralized Version Control System (CVCS)* as opposed to Git which is a *Distributed Version Control System (DVCS)*.
This means that the version history is stored in a central server and the developer only has the files he is working on in his local machine. The developer must be online, working with the server. When dealing with larger files and codebases, *SVN* outperforms *Git* since the latter requires the checkout of the full repository onto the developer's computer.

###B.1 -  Branching and merging

When it comes to branching, *SVN* creates directories inside a repository. Since several developers can be working in the same branch at the same time, this means that when merging, one's version of the trunk might not reflect other developers' branches, and give rise to conflicts, missing files, etc.

###B.2 - Access controls

*SVN* allows the specification of read and write permissions per file level and per directory level. This can can be used to minimize the risk of conflicts. Of course, the application of this measure might not be possible depending on the project's development process. 

###B.3 - Storage requirements

*SVN* repositories are better at handling large binary files, taking less space than Git.

###B.4 - Design
First let's see a comparison between *git* commands and *svn* commands:

| Git 	| Operation 	| Subversion |
| ------------- |:-------------:| -----:|
| `git clone` 	| Copy a repository	| `svn checkout` |
| `git commit` 	| Record changes to file history 	| `svn commit` |
| `git show` 	| View commit details 	| `svn cat` |
| `git status` 	| Confirm status 	| `svn status` |
| `git diff` 	| Check differences 	| `svn diff` |
| `git log` 	| Check log 	| `svn log` |
| `git add` 	| Addition 	| `svn add` |
| `git mv` 	    | Move 	| `svn mv` |
| `git rm` 	    | Delete 	| `svn rm` |
| `git checkout` 	| Cancel change 	| `svn revert1` |
| `git reset`   | Cancel change 	| `svn revert1` |
| `git branch`   | Make a branch 	| `svn copy2` |
| `git checkout` 	| Switch branch 	| `svn switch` |
| `git merge` 	| Merge 	| `svn merge`
| `git tag` 	| Create a tag 	| `svn copy2`
| `git pull` 	| Update 	| `svn update` |
| `git fetch` 	| Update 	| `svn update` |
| `git push` 	| It is reflected on the remote 	| `svn commit3` |
| `gitignore` 	| Ignore file list 	| `.svnignore` |
    
Now let's see how we could apply this to our project *ca1*.

In *SVN* there's no difference between a tag and a branch. Both are just ordinary directories that are created by copying. To do that the following command is used:

    $ svn copy http://svn.example.com/repos/ca1/trunk \
               http://svn.example.com/repos/ca1/tags/release-1.2.0 \
               -m "Tagging the 1.2.0 release of the 'ca1' project."
    
A folder specific for storing tags, should be created, since if another developer alters it and starts committing to it, it becomes a branch.

When branching, there should be a directory specific for branches in the project structure. To create a new branch the following command is used:

     $ svn copy ^/ca1/basic ^/ca1/branches/email-field \
                -m "Creating a private branch of /ca1/trunk for implementation of email field."

Note that *trunk* is the main line of development in *SVN* or the equivalent of *master* branch in *Git*.

Then to checkout to that branch: 

    $ svn checkout http://svn.example.com/repos/ca1/branches/email-field

Merging works more as a way to synchronize/update our branch with the ongoing changes that other developers may have created in the trunk since this branch was created. 

First we need to see the starting revision of our branch:

    $ svn log -v -r2
    ------------------------------------------------------------------------
    r2 | user | 2020-03-15 07:41:25 -0500 (Fri, 15 Mar 2020) | 1 line
    Changed paths:
       A /ca1/branches/email-field (from /ca1/trunk:1)
    
    Creating a private branch of /ca1/trunk.
    ------------------------------------------------------------------------
    
Then we need to check what the most recent version or revision of the trunk is:

    $ svn log -q -rHEAD http://svn.example.com/repos/ca1/trunk
    ------------------------------------------------------------------------
    r5 | Joao | 2020-03-16 08:04:22 -0500 (Sat, 16 Mar 2020)
    ------------------------------------------------------------------------
    
We can see that the latest revision is numbered "r5". Now we can merge into our branch:

    $ svn merge http://svn.example.com/repos/calc/trunk -r340:351

It is good practice to keep a record of this merge so that we don't accidentally try to merge these same changes. For that we record that information in the log message for the commit of the merge:

    $ svn commit -m "Sync the email-field with ^/ca1/trunk through r5."

*SVN* does not require a push command to upload files to the repository. *svn commit* automatically pushes your changes to the server.

To switch branches in *SVN* we use the *svn switch* command. It transforms our existing working copy to reflect a different branch. To do this, we first change into the root directory of our project:

    $ cd ca1
    
Then we confirm in which branch we currently are:

    $ svn info | grep URL
    URL: http://svn.example.com/repos/ca1/trunk
    
And now we can change branches:

    $ svn switch ^/ca1/branches/fix-invalid-email
    
And just to confirm we are in the correct branch:
    
    $ svn info | grep URL
    URL: http://svn.example.com/repos/ca1/branches/fix-invalid-email
    
##### Conclusion
In conclusion, *Git* and *SVN* work different ways. This can be seen when we talk about branching, merging, or tagging. There is also a big difference if we think about the server architecture, since *Subversion* allows the developer to only have the files he is currently working on in his machine, whereas, in *Git* the developer nees to clone the entire project into his machine.

# C - References

https://www.perforce.com/blog/vcs/git-vs-svn-what-difference

http://svnbook.red-bean.com/nightly/en/index.html

https://backlog.com/git-tutorial/reference/commands/
