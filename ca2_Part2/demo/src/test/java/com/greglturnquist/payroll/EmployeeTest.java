package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    /**
     * Test for Employee Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for the constructor - happy case")
    void EmployeeConstructorHappyCaseTest() {
        Employee testEmployee = new Employee("test", "employee", "just a test");
        assertTrue(testEmployee instanceof Employee);
    }

    @Test
    void getFirstNameHappyCaseTest() {
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer");
        String expected = "Frodo";
        String result = employee.getFirstName();
        assertEquals(expected,result);
    }

}