Gradle Basic Demo
===================

This is a demo application that implements a basic multithreaded chat room server.

The server supports several simultaneous clients through multithreading. When a client connects the server requests a screen name, and keeps requesting a name until a unique one is received. After a client submits a unique name, the server acknowledges it. Then all messages from that client will be broadcast to all other clients that have submitted a unique screen name. A simple "chat protocol" is used for managing a user's registration/leaving and message broadcast.


Prerequisites
-------------

 * Java JDK 8
 * Apache Log4J 2
 * Gradle 6.6 (if you do not use the gradle wrapper in the project)
   

Build
-----

To build a .jar file with the application:

    % ./gradlew build 

Run the server
--------------

Open a terminal and execute the following command from the project's root directory:

    % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>

Substitute <server port> by a valid por number, e.g. 59001

Run a client
------------

Open another terminal and execute the following gradle task from the project's root directory:

    % ./gradlew runClient

The above task assumes the chat server's IP is "localhost" and its port is "59001". If you whish to use other parameters please edit the runClient task in the "build.gradle" file in the project's root directory.

To run several clients, you just need to open more terminals and repeat the invocation of the runClient gradle task.

------------
#CA2 Part 1
##A - Analysis, Design and Implementation

###A1 - Copying the example project

First we need to create a copy of the project this assignment was based on. We need to download the project and  upload it to our repository.

So, after downloading the project, we create a new folder named "ca2_Part1" in our local copy of our repository. Then we can upload this new folder with our newly added project:

    git commit -A -m "Cloned given example application to private repository"
    git push
    
--------------
###A2 - Follow the README.md

Now we follow the instructions available in this readme.md file in the sections *Build*, *Run the server* and *Run a client*.
For convenience, the commands are as follows:

    ./gradlew build
     
    % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>
    
    % ./gradlew runClient
    
--------------
###A3 - Add a new task to execute the server.

In the *build.gradle* file we need to add a new task:

    task runServer(type:JavaExec, dependsOn: classes){
        group = "DevOps"
        description = "Launches a chat server localhost:59001 "
    
        classpath = sourceSets.main.runtimeClasspath
    
        main = 'basic_demo.ChatClientApp'
    
        args '59001'
    }

We can now check if this new task is listed along with all the others:

    ./gradlew tasks --all
    
It is now accessible to execution through the following command:

    ./gradlew runServer
    
--------------
###A4 - Add a unit test and update Gradle script

First we create a test class in *src/test/java/basic_demo/*

We then copy the test from the class slides and paste it in the AppTest class.

Now we need to update the dependencies of the project in the *build.gradle* file, because the unit tests require junit 4.12 to execute:

    dependencies {
        // Use Apache Log4J for logging
        compile group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
        compile group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
        testCompile group: 'junit', name: 'junit', version: '4.12'
    }
    
--------------
###A5 - Add a task to make a backup of the sources

Now we add a task of type *"Copy"* to copy the contents of the *src* folder to a new *backup* folder:

    task backup (type: Copy){
        from 'src'
        into 'backup'
    }
    
--------------
###A6 - Add a task to make an archive of the sources

Now we add a task of type *"Zip"* to copy the contents of the *src* folder to a new *"src.zip"* file in *zipfiles* folder:

    task zipFile (type:Zip) {
        from 'src'
        archiveFileName = 'src.zip'
        destinationDirectory = file('zipfiles')
    }
    
--------------
### Conclusion
After the completion of this report, a new tag was created to mark this assignment, and the report and the tag were uploaded to the repository: 

    git tag ca2-part1
    git push origin ca2-part1
    
    git commit -a -m "Final version of README.md"
    git push